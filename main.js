const { app, BrowserWindow, ipcMain, session, Menu } = require("electron");
const argv = require("argv");

const TARGET = "target";

const { options } = argv
  .option([
    {
      name: TARGET,
      type: "string",
      short: "t",
      description: "Target site for UI thread",
    },
  ])
  .run(process.argv);

const rootUiUrl = options[TARGET];

const createWindow = () => {
  const window = new BrowserWindow({
    title: "CTS",
  });
  window.maximize();
  window.loadURL(rootUiUrl);
};

app.commandLine.appendSwitch("ignore-certificate-errors");

app.whenReady().then(() => {
  session.defaultSession.allowNTLMCredentialsForDomains("*");
  Menu.setApplicationMenu(null);
  createWindow();
});

const createAuthPrompt = () => {
  const authPromptWin = new BrowserWindow({
    width: 300,
    height: 400,
    resizable: false,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });
  authPromptWin.loadFile("login.html");

  return new Promise((resolve, reject) => {
    ipcMain.once("formSubmit", (event, credentials) => {
      authPromptWin.close();
      resolve(credentials);
    });
  });
};

app.on("login", (event, webContents, request, authInfo, callback) => {
  event.preventDefault();

  createAuthPrompt().then((credentials) => {
    callback(credentials.username, credentials.password);
  });
});

app.on("window-all-closed", () => {
  app.quit();
});
